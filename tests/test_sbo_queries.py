import unittest


from slpkg.load_data import LoadData


class TestSBoQueries(unittest.TestCase):
    """Test for SBo queries."""

    def setUp(self) -> None:
        """Set the test."""
        load = LoadData()
        self.data: dict = load.load('sbo')
        self.name: str = 'slpkg'

    def test_slackbuild(self) -> None:
        """Test package slackbuild."""
        sbo_name: bool = True
        self.assertTrue(sbo_name, self.data[self.name])

    def test_location(self) -> None:
        """Test package location."""
        self.assertEqual('system', self.data[self.name]['location'])

    def test_sources_x86(self) -> None:
        """Test sources for x86 arch."""
        self.assertEqual(['https://gitlab.com/dslackw/slpkg/-/archive'
                          '/4.9.8/slpkg-4.9.8.tar.gz'], self.data[self.name]['download'])

    def test_sources_x86_64(self) -> None:
        """Test sources for x64 arch."""
        self.assertEqual([], self.data[self.name]['download64'])

    def test_requires(self) -> None:
        """Test for package requires."""
        self.assertEqual(['python3-build', 'python3-pythondialog'],
                         self.data[self.name]['requires'])

    def test_version(self) -> None:
        """Test for package version."""
        self.assertEqual('5.0.4', self.data[self.name]['version'])

    def test_checksum_x86(self) -> None:
        """Test package checksum for x86."""
        self.assertListEqual(['9f9cf626d7246202886774c6cbc2cccf'], self.data[self.name]['md5sum'])

    def test_checksum_x86_64(self) -> None:
        """Test package checksum for x64."""
        self.assertListEqual([], self.data[self.name]['md5sum64'])

    def test_files(self) -> None:
        """Test for package files."""
        self.assertEqual(5, len(self.data[self.name]['files']))

    def test_description(self) -> None:
        """Test for package description."""
        self.assertEqual('slpkg (Slackware Packaging Tool)', self.data[self.name]['description'])


if __name__ == '__main__':
    unittest.main()
