import unittest
from slpkg.checks import Check
from slpkg.load_data import LoadData


class TestPkgInstalled(unittest.TestCase):
    """Test for installed."""

    def setUp(self) -> None:
        """Set the test."""
        load = LoadData()
        self.data = load.load('sbo')
        self.check = Check('sbo')
        self.packages = ['colored', 'sbo-create', 'sun']

    def test_check_exists(self) -> None:
        """Check if packages exist."""
        self.assertIsNone(self.check.package_exists_in_the_database(self.packages, self.data))

    def test_check_is_installed(self) -> None:
        """Check if installed."""
        self.assertIsNone(self.check.is_package_installed(self.packages))


if __name__ == "__main__":
    unittest.main()
