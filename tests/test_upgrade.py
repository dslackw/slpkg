import unittest

from slpkg.upgrade import Upgrade
from slpkg.utilities import Utilities
from slpkg.load_data import LoadData


class TestUpgrade(unittest.TestCase):
    """Test for upgrade."""

    def setUp(self) -> None:
        """Set the test."""
        self.load = LoadData()
        self.utils = Utilities()
        self.data: dict = self.load.load('sbo')

    def test_installed_is_upgradable_for_sbo_repository(self) -> None:
        """Test for sbo installed and upgradeable packages."""
        packages: list = ['sbo-create', 'ptpython', 'pycharm', 'powerline-status']
        is_upgradeable: bool = False
        for pkg in packages:
            self.assertFalse(is_upgradeable, Upgrade('sbo', self.data).is_package_upgradeable(pkg))

    def test_installed_is_upgradable_for_slack_patches_repository(self) -> None:
        """Test for slack installed and upgradeable packages."""
        repo: str = 'slack_patches'
        data: dict = self.load.load(repo)
        packages: list = ['vim', 'httpd', 'seamonkey', 'sudo', 'python3', 'qt5', 'php']
        is_upgradeable: bool = False
        for pkg in packages:
            self.assertFalse(is_upgradeable, Upgrade('slack', data).is_package_upgradeable(pkg))

    def test_installed_is_upgradable_for_alien_repository(self) -> None:
        """Test for alien installed and upgradeable packages."""
        repo: str = 'alien'
        data: dict = self.load.load(repo)
        packages: list = ['audacity', 'vlc', 'dnspython']
        is_upgradeable: bool = False
        for pkg in packages:
            self.assertFalse(is_upgradeable, Upgrade('alien', data).is_package_upgradeable(pkg))


if __name__ == '__main__':
    unittest.main()
