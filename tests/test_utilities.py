import unittest

from slpkg.configs import Configs
from slpkg.utilities import Utilities
from slpkg.load_data import LoadData


class TestUtilities(unittest.TestCase):
    """Test for utilities."""

    def setUp(self) -> None:
        """Set the test."""
        self.utils = Utilities()
        self.repository: str = 'slack'
        load = LoadData()
        self.data: dict = load.load(self.repository)
        self.build_path = Configs.build_path
        self.package = 'aaa_base-15.0-x86_64-4_slack15.0'

    def test_ins_installed(self) -> None:
        """Test fro installed."""
        self.assertEqual(self.package, self.utils.is_package_installed('aaa_base'))

    def test_split_name(self) -> None:
        """Test for name split."""
        self.assertEqual('aaa_base', self.utils.split_package(self.package)['name'])

    def test_split_version(self) -> None:
        """Test for version split."""
        self.assertEqual('15.0', self.utils.split_package(self.package)['version'])

    def test_split_arch(self) -> None:
        """Test for arch split."""
        self.assertEqual('x86_64', self.utils.split_package(self.package)['arch'])

    def test_split_build(self) -> None:
        """Test for build split."""
        self.assertEqual('4', self.utils.split_package(self.package)['build'])

    def test_is_installed(self) -> None:
        """Test for is installed."""
        self.assertEqual(self.package, self.utils.is_package_installed('aaa_base'))

    def test_all_installed(self) -> None:
        """Test for all installed."""
        self.assertIn(self.package, self.utils.all_installed().values())

    def test_is_option(self) -> None:
        """Test for is option."""
        is_parallel: bool = True
        self.assertTrue(is_parallel, self.utils.is_option(('-P', '--parallel'),
                                                          ['-k', '-p', '-P', '--parallel', '--repository']))

    def test_get_file_size(self) -> None:
        """Test for file size."""
        self.assertEqual(1249, self.utils.count_file_size('aaa_base'))

    def test_apply_package_pattern(self) -> None:
        """Test for apply pattern."""
        self.assertGreater(len(self.utils.apply_package_pattern(self.data, ['*'])), 1)


if __name__ == '__main__':
    unittest.main()
