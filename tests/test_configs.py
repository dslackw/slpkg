import unittest
from pathlib import Path
from slpkg.configs import Configs


class TestConfigs(unittest.TestCase):
    """Testing the configuration."""

    def setUp(self) -> None:
        """Set the test."""
        self.configs = Configs

    def test_configs(self) -> None:
        """Test the configs."""
        self.assertEqual('slpkg', self.configs.prog_name)
        self.assertEqual('x86_64', self.configs.os_arch)
        self.assertEqual(Path('/tmp'), self.configs.tmp_path)
        self.assertEqual(Path('/tmp/slpkg'), self.configs.tmp_slpkg)
        self.assertEqual(Path('/tmp/slpkg/build'), self.configs.build_path)
        self.assertEqual(Path('/tmp/slpkg/'), self.configs.download_only_path)
        self.assertEqual(Path('/etc/slpkg'), self.configs.etc_path)
        self.assertEqual(Path('/var/lib/slpkg'), self.configs.lib_path)
        self.assertEqual(Path('/var/log/slpkg/'), self.configs.log_path)
        self.assertEqual(Path('/var/log/packages'), self.configs.log_packages)

        self.assertEqual(Path('/var/log/slpkg/deps.log'), self.configs.deps_log_file)
        self.assertEqual(Path('/var/log/slpkg/slpkg.log'), self.configs.slpkg_log_file)
        self.assertEqual(Path('/var/log/slpkg/upgrade.log'), self.configs.upgrade_log_file)

        self.assertEqual('.pkgs', self.configs.file_list_suffix)
        self.assertEqual(['.tgz', '.txz'], self.configs.package_type)
        self.assertEqual('upgradepkg --install-new', self.configs.installpkg)
        self.assertEqual('upgradepkg --reinstall', self.configs.reinstall)
        self.assertEqual('removepkg', self.configs.removepkg)
        self.assertEqual(True, self.configs.colors)
        self.assertEqual('-j4', self.configs.makeflags)
        self.assertEqual(False, self.configs.gpg_verification)
        self.assertEqual(True, self.configs.checksum_md5)
        self.assertEqual(True, self.configs.dialog)
        self.assertEqual(True, self.configs.view_missing_deps)
        self.assertEqual(False, self.configs.package_method)
        self.assertEqual(False, self.configs.downgrade_packages)
        self.assertEqual(False, self.configs.delete_sources)
        self.assertEqual('wget', self.configs.downloader)
        self.assertEqual('-c -q --progress=bar:force:noscroll --show-progress', self.configs.wget_options)
        self.assertEqual('', self.configs.curl_options)
        self.assertEqual('-c get -e', self.configs.lftp_get_options)
        self.assertEqual('-c mirror --parallel=100 --only-newer --delete', self.configs.lftp_mirror_options)
        self.assertEqual(True, self.configs.ascii_characters)
        self.assertEqual(True, self.configs.ask_question)
        self.assertEqual(True, self.configs.kernel_version)
        self.assertEqual(False, self.configs.parallel_downloads)
        self.assertEqual(5, self.configs.maximum_parallel)
        self.assertEqual(False, self.configs.progress_bar_conf)
        self.assertEqual('spinner', self.configs.progress_spinner)
        self.assertEqual('green', self.configs.spinner_color)
        self.assertEqual('bold_green', self.configs.border_color)
        self.assertEqual(True, self.configs.process_log)

        self.assertEqual(False, self.configs.urllib_retries)
        self.assertEqual(False, self.configs.urllib_redirect)
        self.assertEqual(3.0, self.configs.urllib_timeout)

        self.assertEqual('', self.configs.proxy_address)
        self.assertEqual('', self.configs.proxy_username)
        self.assertEqual('', self.configs.proxy_password)


if __name__ == '__main__':
    unittest.main()
