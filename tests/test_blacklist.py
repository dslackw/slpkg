import unittest
from slpkg.blacklist import Blacklist


class TestBlacklist(unittest.TestCase):
    """Test for blacklist files."""

    def test_blacklist(self) -> None:
        """Test blacklist packages."""
        black = Blacklist()
        self.assertListEqual([], black.packages())


if __name__ == '__main__':
    unittest.main()
