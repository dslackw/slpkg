import unittest

from slpkg.load_data import LoadData


class TestBinQueries(unittest.TestCase):
    """Test for binaries repository."""

    def setUp(self) -> None:
        """Set the test."""
        load = LoadData()
        self.repo_data = load.load('slack')

    def test_repository_data(self) -> None:
        """Test repository data."""
        self.assertGreater(len(list(self.repo_data.keys())), 1)

    def test_package_name(self) -> None:
        """Test for package name."""
        exist: bool = True
        self.assertTrue(exist, self.repo_data.get('aaa_base'))

    def test_version(self) -> None:
        """Test for package version."""
        self.assertEqual('15.0', self.repo_data['aaa_base']['version'])

    def test_package_bin(self) -> None:
        """Test for binary package."""
        self.assertEqual('aaa_base-15.0-x86_64-3.txz', self.repo_data['aaa_base']['package'])

    def test_mirror(self) -> None:
        """Test for mirror."""
        self.assertEqual('http://mirror.nl.leaseweb.net/slackware/slackware64-15.0/',
                         self.repo_data['aaa_base']['mirror'])

    def test_location(self) -> None:
        """Test for location."""
        self.assertEqual('slackware64/a', self.repo_data['aaa_base']['location'])

    def test_size_comp(self) -> None:
        """Test for comp size."""
        self.assertEqual('12', self.repo_data['aaa_base']['size_comp'])

    def test_size_uncomp(self) -> None:
        """Test for uncomp size."""
        self.assertEqual('90', self.repo_data['aaa_base']['size_uncomp'])

    def test_required(self) -> None:
        """Test for requires."""
        self.assertEqual([], self.repo_data['aaa_base']['requires'])

    def test_conflicts(self) -> None:
        """Test for conflicts."""
        self.assertEqual('', self.repo_data['aaa_base']['conflicts'])

    def test_suggests(self) -> None:
        """Test for suggests."""
        self.assertEqual('', self.repo_data['aaa_base']['suggests'])

    def test_description(self) -> None:
        """Test for description."""
        self.assertEqual('', self.repo_data['aaa_base']['description'])

    def test_package_checksum(self) -> None:
        """Test for package checksum."""
        self.assertEqual('ee674755e75a3f9cb3c7cfc0039f376d', self.repo_data['aaa_base']['checksum'])


if __name__ == '__main__':
    unittest.main()
